﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using DataModel;
using System.Reflection;

namespace Logic
{
    public class BotLogic
    {
       
        private static string token = "590476590:AAEZYhGTsLVPD1PUZg3FyZi9avIX6qxx_PE";
        private static TelegramBotClient client;
        public static void StatBot()
        {
            client = new TelegramBotClient(token);
            client.OnMessage += BotOnMessageReceived;
            client.OnMessageEdited += BotOnMessageReceived;
            client.StartReceiving();
            Thread notifier = new Thread(new ThreadStart(SendNotifications));
            notifier.Start();
        }
        private static void SendNotifications()
        {
            if (DateTime.Now.Minute != 0)
            {
                Thread.Sleep(new TimeSpan(0, 60 - DateTime.Now.Minute, 0));
            }
            while (true)
            {
                
                DateTime curTime = DateTime.Now;
                foreach (BotUser bu in Context.ContextProvider.Users)
                {
                    List<Plan> plansNow = bu.Plans.Where(p => p.PlanDateTime.Date == curTime.Date && p.PlanDateTime.Hour == curTime.Hour).OrderBy(p => p.PlanDateTime).ToList();
                    if (plansNow.Count != 0)
                    {
                        string answer = "Запланированные задачи на этот час:\n";
                        foreach (Plan p in plansNow)
                        {
                            answer += p.PlanDateTime.TimeOfDay + "\n" + p.Text + "\n";
                        }
                        client.SendTextMessageAsync(bu.TUserId, answer);
                    }
                }
                Thread.Sleep(new TimeSpan(0, 60-DateTime.Now.Minute, 0));
            }
        }
        public static void OffBot()
        {
            client.StopReceiving();
        }
        public static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            if (message?.Type == MessageType.Text)
            {
                switch (messageEventArgs.Message.Text)
                {
                    case "/start": await client.SendTextMessageAsync(message.Chat.Id, "Возможные операции: \n /addPlan \n /checkToday \n /checkWeek \n /checkDate \n /deletePlan \n /cancelOperation \n /help"); break;
                    case "/addPlan": AddPlan(sender, messageEventArgs); break;
                    case "/checkToday": CheckToday(sender, messageEventArgs); break;
                    case "/checkWeek": CheckWeek(sender, messageEventArgs); break;
                    case "/deletePlan": DeletePlan(sender, messageEventArgs); break;
                    case "/checkDate": CheckPlanDate(sender, messageEventArgs);break;
                    case "/cancelOperation":CancelOperation(sender, messageEventArgs); break;
                    case "/help": await client.SendTextMessageAsync(message.Chat.Id, "Возможные операции: \n /addPlan \n /checkToday \n /checkWeek \n /checkDate \n /deletePlan \n /cancelOperation \n /help"); break;
                    default: DoReflection(sender, messageEventArgs); break;
                }
            }
           
        }

        private static void CancelOperation(object sender, MessageEventArgs messageEventArgs)
        {
            BotUser curUser = GetCurUser(messageEventArgs);
            if (curUser != null)
            {
                curUser.NextMethod = "";
                curUser.CurrentInput = "";
                Context.ContextProvider.SaveChanges();
                SendAnswer(messageEventArgs, "Все операции остановлены!");
            }
        }

        private static void CheckPlanDate(object sender, MessageEventArgs messageEventArgs)
        {
            BotUser curUser = GetCurUser(messageEventArgs);
            if (curUser == null)
            {
                SendAnswer(messageEventArgs, "У вас еще нет запланированных задач!");
                return;
            }
            SendAnswer(messageEventArgs, "Введите дату в формате гггг мм дд все через пробел");
            curUser.NextMethod = "FinishCheckDate";
            Context.ContextProvider.SaveChanges();
            
        }
        public static void FinishCheckDate(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message.Text;
            string[] splitedDate = message.Split(' ');
            DateTime dt = new DateTime();
            if (splitedDate.Length == 3 && CheckDate(splitedDate, out dt,true))
            {
                BotUser curUser = GetCurUser(messageEventArgs);
                curUser.NextMethod = "";
                CheckPlanHelper(messageEventArgs, curUser, dt);
                Context.ContextProvider.SaveChanges();
            }
            else
            {
                SendAnswer(messageEventArgs, "Неверно введена дата, попробуйте повторить ввод!");
            }
        }
        private static void DoReflection(object sender, MessageEventArgs messageEventArgs)
        {
            try {
                BotUser curUser = GetCurUser(messageEventArgs);
                if (curUser.NextMethod == "") return;
                Type type = typeof(BotLogic);
                object o = Activator.CreateInstance(type);
                MethodInfo info = type.GetMethod(curUser.NextMethod);
                info.Invoke(o, new object[] { sender, messageEventArgs });
            }
            catch (Exception e) { Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        private static void AddPlan(object sender, MessageEventArgs messageEventArgs)
        {
            SendAnswer(messageEventArgs, "Введите дату в формате гггг мм дд часы минуты все через пробел");
            BotUser curUser = GetCurUser(messageEventArgs);
            if (curUser!= null)
            {
                curUser.NextMethod = "AddPlanDate";
                Context.ContextProvider.SaveChanges();
            }
            else
            {
                Context.ContextProvider.Users.Add(new BotUser()
                {
                    TUserId=messageEventArgs.Message.From.Id,
                    User = messageEventArgs.Message.From,
                    Plans =new List<Plan>(),NextMethod="AddPlanDate"
                });
                Context.ContextProvider.SaveChanges();
            }
        }
        public static void AddPlanDate(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message.Text;
            string[] splitedDate = message.Split(' ');
            DateTime dt = new DateTime();
            if (splitedDate.Length == 5 && CheckDate(splitedDate,out dt,false)) 
            {
                BotUser curUser = GetCurUser(messageEventArgs);
                curUser.CurrentInput = message;
                curUser.NextMethod = "AddPlanText";
                Context.ContextProvider.SaveChanges();
                SendAnswer(messageEventArgs, "Введите текст задачи");
            }
            else
            {
                SendAnswer(messageEventArgs, "Неверно введена дата, попробуйте повторить ввод!");
            }
        }
        public static void AddPlanText(object sender, MessageEventArgs messageEventArgs)
        {
            BotUser curUser = GetCurUser(messageEventArgs);
            var message = messageEventArgs.Message.Text;
            string[] splitedInput = curUser.CurrentInput.Split(' ');
            DateTime dt = new DateTime();
            CheckDate(splitedInput,out dt,false);
            curUser.Plans.Add(new Plan() { PlanDateTime = dt, Text = message,ChatId=(int)messageEventArgs.Message.Chat.Id});
            curUser.NextMethod = "";
            curUser.CurrentInput = "";
            Context.ContextProvider.SaveChanges();
            client.SendTextMessageAsync(messageEventArgs.Message.Chat.Id, "Задача добавлена!");
        }
        private static void CheckToday(object sender, MessageEventArgs messageEventArgs)
        {
            BotUser curUser = GetCurUser(messageEventArgs);
            if (curUser != null)
            {
                CheckPlanHelper(messageEventArgs, curUser, messageEventArgs.Message.Date);
            }
            else
            {
                SendAnswer(messageEventArgs, "Нет запланированных задач");
            }
          
        }
        private static void CheckWeek(object sender, MessageEventArgs messageEventArgs)
        {
            BotUser curUser = GetCurUser(messageEventArgs);
            if (curUser == null)
            {
                SendAnswer(messageEventArgs, "У вас еще нет задач!");
                return;
            }
            DateTime curDate = messageEventArgs.Message.Date;
            List<Plan> plans = curUser.Plans.Where(u => u.PlanDateTime >= curDate && u.PlanDateTime <= curDate.AddDays(7)).OrderBy(u=>u.PlanDateTime.Ticks).ToList();
            string answer = "";
            string curDayOfWeek = "";
            foreach (Plan p in plans)
            {
                if (curDayOfWeek != p.PlanDateTime.DayOfWeek.ToString())
                {
                    curDayOfWeek = p.PlanDateTime.DayOfWeek.ToString();
                    answer += p.PlanDateTime.DayOfWeek + "\n" + p.PlanDateTime.TimeOfDay+ "\n" + p.Text + "\n";
                }
                else
                {
                    answer += p.PlanDateTime.TimeOfDay + "\n" + p.Text + "\n";
                }
            }
            SendAnswer(messageEventArgs, answer);
        }
        private static void DeletePlan(object sender, MessageEventArgs messageEventArgs)
        {
            BotUser curUser = GetCurUser(messageEventArgs);
            if (curUser == null)
            {
                SendAnswer(messageEventArgs, "У вас еще нет запланированных задач!");
                return;
            }
            SendAnswer(messageEventArgs, "Введите дату в формате гггг мм дд все через пробел");
            curUser.NextMethod = "ChooseDeletePlan";
            Context.ContextProvider.SaveChanges();
        }
        public static void ChooseDeletePlan(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message.Text;
            string[] splitedDate = message.Split(' ');
            DateTime dt = new DateTime();
            if (splitedDate.Length == 3 && CheckDate(splitedDate, out dt, true))
            {
                var curUser = GetCurUser(messageEventArgs);
                string answer = "";
                List<Plan> plans = curUser.Plans.Where(u => u.PlanDateTime.Date == dt).OrderBy(t=>t.PlanDateTime.Ticks).ToList();
                if (plans.Count != 0)
                {
                    curUser.CurrentInput = "";
                    for (int i = 0; i < plans.Count; i++)
                    {
                        curUser.CurrentInput += plans[i].Id+" ";
                        answer += "Номер:" + i + "\n" + plans[i].PlanDateTime.TimeOfDay + "\n" + plans[i].Text + "\n";
                    }
                    answer += "Введите номер плана, который хотели бы удалить";
                    SendAnswer(messageEventArgs, answer);
                    curUser.NextMethod = "FinishDeletePlan";
                    Context.ContextProvider.SaveChanges();
                }
                else
                {
                    SendAnswer(messageEventArgs, "У вас нет планов на эту дату!");
                    curUser.CurrentInput = "";
                    curUser.NextMethod = "";
                    Context.ContextProvider.SaveChanges();
                }
            }
            else
            {
                SendAnswer(messageEventArgs, "Неверно введена дата, попробуйте повторить ввод!");
            }
        }
        public static void FinishDeletePlan(object sender, MessageEventArgs messageEventArgs)
        {
            int i;
            if (int.TryParse(messageEventArgs.Message.Text, out i))
            {
                var curUser = GetCurUser(messageEventArgs);
                string[] splitedInput = curUser.CurrentInput.Split(' ');
                if (i < splitedInput.Length && i >= 0)
                {
                    curUser.Plans.Remove(curUser.Plans.First(u => u.Id == int.Parse(splitedInput[i])));
                    curUser.CurrentInput = "";
                    curUser.NextMethod = "";
                    Context.ContextProvider.SaveChanges();
                    SendAnswer(messageEventArgs, "Успешно удалено!");
                }
                else
                {
                    SendAnswer(messageEventArgs, "Неверный номер! Попробуйте еще раз");
                }
            }
            else
            {
                SendAnswer(messageEventArgs, "Неверный номер! Попробуйте еще раз");
            }

        }
        private static void CheckPlanHelper(MessageEventArgs messageEventArgs,BotUser curUser,DateTime dt)
        {
            var plans = curUser.Plans.Where(u => u.PlanDateTime.Date == dt.Date &&
              u.PlanDateTime.Ticks >= dt.Ticks).OrderBy(u=>u.PlanDateTime).ToList();
            if (plans.Count == 0)
            {
                SendAnswer(messageEventArgs, "Нет запланированных задач");
            }
            else
            {
                string answer="";
                foreach (Plan p in plans)
                {
                    answer += p.PlanDateTime.TimeOfDay.ToString() + "\n " + p.Text + "\n";
                }
                SendAnswer(messageEventArgs, answer);
            }
        }
        private static bool CheckDate(string[] splitedInput,out DateTime inputedDate,bool dateOnly)
        {
            try
            {
                List<int> parsedDate = new List<int>();
                foreach (string s in splitedInput)
                {
                    parsedDate.Add(int.Parse(s));
                }
                if (dateOnly)
                {
                    inputedDate = new DateTime(parsedDate[0], parsedDate[1], parsedDate[2]);
                    return true;
                }
                inputedDate = new DateTime(parsedDate[0], parsedDate[1], parsedDate[2], parsedDate[3], parsedDate[4], 0);
                return true;
            }
            catch
            {
                inputedDate = new DateTime();
                return false;
            }
        }
        private static BotUser GetCurUser(MessageEventArgs messageEventArgs)
        {
            return Context.ContextProvider.Users.FirstOrDefault(u => u.TUserId == messageEventArgs.Message.From.Id);
        }
        private static void SendAnswer(MessageEventArgs messageEventArgs,string answer)
        {
           client.SendTextMessageAsync(messageEventArgs.Message.Chat.Id, answer);
        }
    }
}
