namespace Logic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idFixes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BotUsers", "TUserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BotUsers", "TUserId");
        }
    }
}
