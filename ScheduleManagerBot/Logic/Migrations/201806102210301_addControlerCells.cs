namespace Logic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addControlerCells : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BotUsers", "CurrentInput", c => c.String());
            AddColumn("dbo.BotUsers", "NextMethod", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BotUsers", "NextMethod");
            DropColumn("dbo.BotUsers", "CurrentInput");
        }
    }
}
