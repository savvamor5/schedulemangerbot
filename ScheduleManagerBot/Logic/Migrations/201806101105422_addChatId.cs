namespace Logic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addChatId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Plans", "ChatId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Plans", "ChatId");
        }
    }
}
