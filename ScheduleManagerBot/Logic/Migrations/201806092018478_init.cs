namespace Logic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BotUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Plans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlanDateTime = c.DateTime(nullable: false),
                        Text = c.String(),
                        BotUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BotUsers", t => t.BotUser_Id)
                .Index(t => t.BotUser_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsBot = c.Boolean(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Username = c.String(),
                        LanguageCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BotUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Plans", "BotUser_Id", "dbo.BotUsers");
            DropIndex("dbo.Plans", new[] { "BotUser_Id" });
            DropIndex("dbo.BotUsers", new[] { "User_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.Plans");
            DropTable("dbo.BotUsers");
        }
    }
}
