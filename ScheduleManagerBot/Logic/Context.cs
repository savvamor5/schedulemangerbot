﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Logic
{
    public class Context:DbContext
    {
        public static Context ContextProvider { get; set; } = new Context();
        public DbSet<DataModel.BotUser> Users { get; set; }
        public Context() : base("tldbScheduleBot")
        {

        }
    }
}
