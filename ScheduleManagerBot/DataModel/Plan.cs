﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Plan
    {
        public int Id { get; set; }
        public DateTime PlanDateTime { get; set; }
        public string Text { get; set; }
        public int ChatId { get; set; }
    }
}
