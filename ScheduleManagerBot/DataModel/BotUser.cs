﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public  class BotUser
    {
        public int Id { get; set; }
        public int TUserId { get; set;}
        public Telegram.Bot.Types.User User { get; set; }
        public virtual List<Plan> Plans { get; set; }
        public string CurrentInput { get; set; }
        public string NextMethod { get; set; }
    }
}
